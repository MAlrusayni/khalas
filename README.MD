# Rewrite (Updates)
Hi, This project have been rewritten and moved into another repo with new name (Savory), you can find Savory here
**[Savory](https://gitlab.com/MAlrusayni/savory)**

# Khalas
**Rust / Wasm frontend UI library for building user interfaces.**

[![master docs](https://img.shields.io/badge/docs-master-blue.svg)](https://malrusayni.gitlab.io/khalas/khalas/)
&middot;
[![crate info](https://img.shields.io/crates/v/khalas.svg)](https://crates.io/crates/khalas)
&middot;
[![pipeline](https://gitlab.com/MAlrusayni/khalas/badges/master/pipeline.svg)](https://gitlab.com/MAlrusayni/khalas/pipelines)
&middot;
[![rustc version](https://img.shields.io/badge/rustc-stable-green.svg)](https://crates.io/crates/khalas)

All documentation can be found in [API Docs](https://malrusayni.gitlab.io/khalas/khalas/)

## Screenshot
these screenshots will represnte current state for this proejct

![Screenshot](screenshot.png)

#### License

Licensed under either of <a href="LICENSE-APACHE">Apache License, Version
2.0</a> or <a href="LICENSE-MIT">MIT license</a> at your option.

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in Khalas by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
